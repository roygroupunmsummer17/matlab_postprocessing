function temp_mov(Tscale,slicevx,slicevy,slicevz,number,...
                  base,file,file1,file2,slicer,xb,yb,zb,h,...
                  vscale,timestep,st,movie,tau)

% Tscale is the temp scaling
% base is the location folder
% file is a scalar h5 file
% file3 is also a scalar file
% Tr is the node location
% h is the dimensionalization
% xb,yb,zb is the scaling for its respective axis
% nodes is the # of nodes

figure(number)

[X, ~, ~, shape] = read_geometry(base,file);   % reads the geometry

[sx sy sz] = size(X);
ux = 0:h*xb/(sx-1):h*xb;
uy = 0:h*yb/(sy-1):h*yb;
uz = 0:h*zb/(sz-1):h*zb;
spacingx = h/(sx-1);
spacingy = h/(sy-1);
spacingz = h/(sz-1);

nodex = slicevx * spacingx;
nodey = slicevy * spacingy;
nodez = slicevz * spacingz; 

[Vx Vy Vz] = read_vector(base, file1, timestep, shape);
Vx = Vx * vscale;
Vy = Vy * vscale;
Vz = Vz * vscale;
[Ux Uy Uz] = read_vector(base, file2, timestep, shape);
Ux = Ux * vscale;
Uy = Uy * vscale;
Uz = Uz * vscale;

rel_x = Vx - Ux;
rel_y = Vy - Uy;
rel_z = Vz - Uz;

[x y z] = meshgrid(0:h*xb/(sx-1):h,0:h*yb/(sy-1):h*yb,0:h*zb/(sz-1):h*zb);

hold on
if movie == 0
    
    tfluid = read_scalar(base,file,timestep,shape);
    tfluid = Tscale * tfluid;

    for i = 1:3
        for j = 1:3
            
            p = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 + spacingx*i,h/2 + spacingy*j,nodez*zb);
            q = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 - spacingx*i,h/2 + spacingy*j,nodez*zb);
            r = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 + spacingx*i,h/2 - spacingy*j,nodez*zb);
            s = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 - spacingx*i,h/2 - spacingy*j,nodez*zb);

            set( p, 'Color', [0 0 0] )
            set( q, 'Color', [0 0 0] )
            set( r, 'Color', [0 0 0] )
            set( s, 'Color', [0 0 0] )

        end
    end
    
    if slicer == 1
        
        T = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);
        T1 = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);

        for i = 0:sy-1
            F = streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
            set( F, 'Color', [0 0 0] )
            drawnow
        end

    elseif slicer == 2
        
        T = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);
        T1 = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);

        for i = 0:sx-1
            F = streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
            set( F, 'Color', [0 0 0] )
            drawnow
        end        

    elseif slicer == 3
        
        T = slice(ux,uy,uz,tfluid,[],[],nodez*zb);
        for i = 0:sx-1
            for j = 0:sy-1
                F = streamline(x,y,z,rel_x,rel_y,rel_z,spacingx*i,spacingy*j,nodez*zb);
                set( F, 'Color', [0 0 0] )
                drawnow
            end
        end
        
    end
    time = timestep * tau * 2 / 3e11;
    colorbar
    T.FaceColor = 'interp';
    T.DiffuseStrength = 0.8;
    T.EdgeColor = [0.5, 0.5 0.5];
    T.EdgeAlpha = [0.5];
    T1.FaceColor = 'interp';
    T1.DiffuseStrength = 0.8;
    T1.EdgeColor = [0.5, 0.5 0.5];
    T1.EdgeAlpha = [0.5];
    view(3)
    xlabel('x')
    ylabel('y')
    zlabel('z')
    title([num2str(time),' Million Year']);
    zlim([0 h*zb])
    xlim([0 h*xb])
    ylim([0 h*yb])
    hold off
        
else
    
    for i = 1:3
        for j = 1:3
            
            p = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 + spacingx*i,h/2 + spacingy*j,nodez*zb);
            q = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 - spacingx*i,h/2 + spacingy*j,nodez*zb);
            r = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 + spacingx*i,h/2 - spacingy*j,nodez*zb);
            s = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 - spacingx*i,h/2 - spacingy*j,nodez*zb);

            set( p, 'Color', [0 0 0] )
            set( q, 'Color', [0 0 0] )
            set( r, 'Color', [0 0 0] )
            set( s, 'Color', [0 0 0] )

        end
    end

    for t = 0:floor(998/st)
        
        tfluid = read_scalar(base,file,timestep,shape);
        tfluid = Tscale * tfluid;
        
        if slicer == 1
            
            T = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);
            T1 = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);
            for i = 0:sy-1
                F = streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
                set( F, 'Color', [0 0 0] )
                drawnow
            end

        elseif slicer == 2
            
            T = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);
            T1 = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);
            for i = 0:sx-1
                streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
                set( F, 'Color', [0 0 0] )
                drawnow
            end

        elseif slicer == 3
            
            T = slice(ux,uy,uz,tfluid,[],[],nodez*zb);
            for i = 0:sx-1
                for j = 0:sy-1
                    streamline(x,y,z,rel_x,rel_y,rel_z,spacingx*i,spacingy*j,nodez*zb);
                    set( F, 'Color', [0 0 0] )
                    drawnow
                end
            end
            
        end

        time = timestep * tau * 2 / 3e11;
        colorbar
        T.FaceColor = 'interp';
        T.DiffuseStrength = 0.8;
        T.EdgeColor = [0.5, 0.5 0.5];
        T.EdgeAlpha = [0.5];
        T1.FaceColor = 'interp';
        T1.DiffuseStrength = 0.8;
        T1.EdgeColor = [0.5, 0.5 0.5];
        T1.EdgeAlpha = [0.5];        title([num2str(time),' Million Years'])
        view(3)
        xlabel('x')
        ylabel('y')
        zlabel('z')
        zlim([0 h*zb])
        xlim([0 h*xb])
        ylim([0 h*yb])
        hold off
        
        timestep = timestep +st

    
    end    
    
end