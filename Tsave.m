function Tsave(Tscale,base,file,file3,...
               Tr,h,xb,yb,zb,nodes,muscale) 
% This code will save the temperature readings at a specific location
% denotated by Tr. Then it will save it by its location name in meters.

% Tscale is the temp scaling
% base is the location folder
% file is a scalar h5 file
% file3 is also a scalar file
% Tr is the node location
% h is the dimensionalization
% xb,yb,zb is the scaling for its respective axis
% nodes is the # of nodes

[~, ~, ~, shape] = read_geometry(base,file);   % reads the geometry
sx = shape(1);
hold on
filename = ['Ext_Data/',base,'TN_hist_',num2str(Tr(1)),'_',num2str(Tr(2)),'_',num2str(Tr(3)),'.mat'];
        Tr(1) = fix(Tr(1) / (h/nodes) / xb) + 1;
        Tr(2) = fix(Tr(2) / (h/nodes) / yb) + 1;
        Tr(3) = fix(Tr(3) / (h/nodes) / zb) + 1;
        
for i = 0:500           % Starts a loop to read the data
    tfluid = read_scalar(base,file,i,shape); % reading of temp
    mu = read_scalar(base,file3,i,shape);    % reading of mu
    mu = muscale * mu;
    tfluid = Tscale * tfluid;                       % temp scaling
    TM_rec(i+1,1) = 2 * i / 1e2;                    % scaled time storing
    TM_rec(i+1,2) = tfluid(Tr(1),Tr(2),Tr(3));      % temp storing
    TM_rec(i+1,3) = mu(Tr(1),Tr(2),Tr(3));          % mu storing
end

% This is all saving logistics. You can edit the name and the location by
% changing a couple things below.

save(filename,'TM_rec')   
    
    
    
end