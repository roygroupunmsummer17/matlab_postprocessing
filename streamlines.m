% This plots the streamlines
function streamlines(vscale,slicevx,slicevy,slicevz,number,...
                     base,file1,file2,slicer,xb,yb,zb,h, ...
                     timestep,st,movie,color)

% Tscale is the temp scaling
% base is the location folder
% file is a scalar h5 file
% file3 is also a scalar file
% Tr is the node location
% h is the dimensionalization
% xb,yb,zb is the scaling for its respective axis
% nodes is the # of nodes

figure(number)

[X, ~, ~, shape] = read_geometry(base,file1);   % reads the geometry
[Vx, Vy, Vz] = read_vector(base, file1, timestep, shape);
Vx = Vx * vscale;
Vy = Vy * vscale;
Vz = Vz * vscale;
[Ux, Uy, Uz] = read_vector(base, file2, timestep, shape);
Ux = Ux * vscale;
Uy = Uy * vscale;
Uz = Uz * vscale;

rel_x = Vx - Ux;
rel_y = Vy - Uy;
rel_z = Vz - Uz;

[sx, sy, sz] = size(X);
spacingx = h/(sx-1);
spacingy = h/(sy-1);
spacingz = h/(sz-1);

nodex = slicevx * spacingx; %This is the slicing plane where the streamlines will start has to be between [0,1]
nodey = slicevy * spacingy; %This is the slicing plane where the streamlines will start has to be between [0,1]
nodez = slicevz * spacingz; %This is the slicing plane where the streamlines will start has to be between [0,1]


[x, y, z] = meshgrid(0:h*xb/(sx-1):h,0:h*yb/(sy-1):h*yb,0:h*zb/(sz-1):h*zb);


hold on
if movie == 0
    if slicer == 1
      
        for i = 0:sy-1
            hold on
            s = streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
            set( s, 'Color', color )
            plot3(s.XData(end),s.YData(end),s.ZData(end),'Color',color,'MarkerSize',4,'Marker','o','MarkerFaceColor',color);
            drawnow
        end

    elseif slicer == 2

        for i = 0:sx-1
            hold on
            s = streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
            set( s, 'Color', color )
            plot3(s.XData(end),s.YData(end),s.ZData(end),'Color',color,'MarkerSize',4,'Marker','o','MarkerFaceColor',color);
            drawnow
        end

    elseif slicer == 3

        for i = 0:sx-1
            for j = 0:sy-1
                hold on
                s = streamline(x,y,z,rel_x,rel_y,rel_z,spacingx*i,spacingy*j,nodez*zb);
                set( s, 'Color', color )
                plot3(s.XData(end),s.YData(end),s.ZData(end),'Color',color,'MarkerSize',4,'Marker','o','MarkerFaceColor',color);
                drawnow
            end
        end
        
    end

    
    time = timestep * 2 / 1e2;

    title([num2str(time),' Million Years'])
    view(3)
    xlabel('x')
    ylabel('y')
    zlabel('z')
    zlim([0 h*zb])
    xlim([0 h*xb])
    ylim([0 h*yb])
    pause(.1)
    hold off

else
    
    for t = 0:floor(998/st)

        if slicer == 1

            for i = 0:sy-1
                s = streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
                set( s, 'Color', color )
                drawnow
            end

        elseif slicer == 2

            for i = 0:sx-1
                streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
                set( s, 'Color', color )
                drawnow
            end

        elseif slicer == 3

            for i = 0:sx-1
                for j = 0:sy-1
                    streamline(x,y,z,rel_x,rel_y,rel_z,spacingx*i,spacingy*j,nodez*zb);
                    set( s, 'Color', color )
                    drawnow
                end
            end
            
        end

        Time = timestep * 2 / 1e2;
        title([num2str(time),' Million Years'])
        view(3)
        xlabel('x')
        ylabel('y')
        zlabel('z')
        zlim([0 h*zb])
        xlim([0 h*xb])
        ylim([0 h*yb])
        hold off
        
        timestep = timestep +st

    
    end
   
    
    
     
    
    
    
    
    
    
end