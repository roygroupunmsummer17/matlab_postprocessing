function temp_mov(Tscale,slicevx,slicevy,slicevz,number,...
                  base,file,file1,file2,slicer,xb,yb,zb,h,...
                  timestep,st,movie,tau)
figure(number)

[X Y Z shape] = read_geometry(base,file);

[sx sy sz] = size(X);
ux = 0:h*xb/(sx-1):h*xb;
uy = 0:h*yb/(sy-1):h*yb;
uz = 0:h*zb/(sz-1):h*zb;
spacingx = h/(sx-1);
spacingy = h/(sy-1);
spacingz = h/(sz-1);

nodex = slicevx * spacingx;
nodey = slicevy * spacingy;
nodez = slicevz * spacingz; 

[Vx Vy Vz] = read_vector(base, file1, timestep, shape);
Vx = Vx * vscale;
Vy = Vy * vscale;
Vz = Vz * vscale;
[Ux Uy Uz] = read_vector(base, file2, timestep, shape);
Ux = Ux * vscale;
Uy = Uy * vscale;
Uz = Uz * vscale;

rel_x = Vx - Ux;
rel_y = Vy - Uy;
rel_z = Vz - Uz;

[x y z] = meshgrid(0:h*xb/(sx-1):h,0:h*yb/(sy-1):h*yb,0:h*zb/(sz-1):h*zb);

hold on
if movie == 0
    
    tfluid = read_scalar(base,file,timestep,shape);
    tfluid = Tscale * tfluid;

    if slicer == 1
        
        T = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);  
        for i = 0:sy-1
            s = streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
            set( s, 'Color', [0 0 0] )
            drawnow
        end

    elseif slicer == 2
        
        T = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);
        for i = 0:sx-1
            s = streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
            set( s, 'Color', [0 0 0] )
            drawnow
        end        

    elseif slicer == 3
        
        T = slice(ux,uy,uz,tfluid,[],[],nodez*zb);
        for i = 0:sx-1
            for j = 0:sy-1
                s = streamline(x,y,z,rel_x,rel_y,rel_z,spacingx*i,spacingy*j,nodez*zb);
                set( s, 'Color', [0 0 0] )
                drawnow
            end
        end
        
    end
    time = timestep * tau * 2 / 3e11;
    colorbar
    T.FaceColor = 'interp';
    T.DiffuseStrength = 0.8;
    T.EdgeColor = [0.5, 0.5 0.5];
    T.EdgeAlpha = [0.5];
    view(3)
    xlabel('x')
    ylabel('y')
    zlabel('z')
    title([num2str(time),' Million Year']);
    zlim([0 h*zb])
    xlim([0 h*xb])
    ylim([0 h*yb])
    hold off
        
else
    
    for t = 0:floor(998/st)
        figure(1)
        view(3)
        xlabel('x')
        ylabel('y')
        zlabel('z')

        tfluid = read_scalar(base,file,timestep,shape);
        tfluid = Tscale * tfluid;
        
        if slicer == 1
            node = ux(slicet); % this choses at which node point
            T = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);     
            zlim([0 h*zb])
            xlim([0 h*xb])
            ylim([0 h*yb])
            for i = 0:sy-1
                streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
                drawnow
            end

        elseif slicer == 2
            node = uy(slicet); % this choses at which node point
            T = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);
            zlim([0 h*zb])
            xlim([0 h*xb])
            ylim([0 h*yb])
            for i = 0:sx-1
                streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
                drawnow
            end

        elseif slicer == 3
            node = uz(slicet); % this choses at which node point
            T = slice(ux,uy,uz,tfluid,[],[],nodez*zb);
            zlim([0 h*zb])
            xlim([0 h*xb])
            ylim([0 h*yb])
            for i = 0:sx-1
                for j = 0:sy-1
                    streamline(x,y,z,rel_x,rel_y,rel_z,spacingx*i,spacingy*j,nodez*zb);
                    drawnow
                end
            end
        end
        
        T_rec(t+1) = tfluid(Tr(1),Tr(2),Tr(3));        

        
        colorbar
        T.FaceColor = 'interp';
        T.DiffuseStrength = 0.8;
        T.EdgeColor = [0.5, 0.5 0.5];
        T.EdgeAlpha = [0.5];
        view(3)
        xlabel('x')
        ylabel('y')
        zlabel('z')

        hold off
        
        timestep = timestep +st

    
    end
    
    
    
    
     
    
    
    
    
    
    
end