% This will plot the temperature profile
function mu_profile(muscale,slicevx,slicevy,slicevz,number,...
                      base,file3,slicer,xb,yb,zb,h,...
                      timestep,st,movie)

% Tscale is the temp scaling
% base is the location folder
% file is a scalar h5 file
% file3 is also a scalar file
% Tr is the node location
% h is the dimensionalization
% xb,yb,zb is the scaling for its respective axis
% nodes is the # of nodes

figure(number)

[X, ~, ~, shape] = read_geometry(base,file3);   % reads the geometry

[sx, sy, sz] = size(X);
ux = 0:h*xb/(sx-1):h*xb;
uy = 0:h*yb/(sy-1):h*yb;
uz = 0:h*zb/(sz-1):h*zb;
spacingx = h/(sx-1);
spacingy = h/(sy-1);
spacingz = h/(sz-1);

nodex = slicevx * spacingx;
nodey = slicevy * spacingy;
nodez = slicevz * spacingz; 

hold on

if movie == 0

    mu = read_scalar(base,file3,timestep,shape);
    mu = permute(mu,[2,1,3]);

    mu = log10(muscale * mu);

    if slicer == 1
        m = slice(ux,uy,uz,mu,nodex*xb,[],[]);        

    elseif slicer == 2
        m = slice(ux,uy,uz,mu,[],nodey*yb,[]);

    elseif slicer == 3
        m = slice(ux,uy,uz,mu,[],[],nodez*zb);

    end
    caxis([15 30])
    c = colorbar('AxisLocation','in');
    c.Label.String = 'Temperature of melt (in C)';
    c.Limits = [15 30];
    m.FaceColor = 'interp';
    m.DiffuseStrength = 0.8;
    m.EdgeColor = [0.5, 0.5 0.5];
    m.EdgeAlpha = [0.5];
    view(3)
    xlabel('x')
    ylabel('y')
    zlabel('z')
    zlim([0 h*zb])
    xlim([0 h*xb])
    ylim([0 h*yb])
    pause(.1)
    hold off

else
    
    for t = 0:floor(1000/st)

        mu = read_scalar(base,file3,timestep,shape);
        mu = permute(mu,[2,1,3]);

        mu = log(muscale * mu);

        if slicer == 1
            m = slice(ux,uy,uz,mu,nodex*xb,[],[]);     

        elseif slicer == 2
            m = slice(ux,uy,uz,mu,[],nodey*yb,[]);


        elseif slicer == 3
            m = slice(ux,uy,uz,mu,[],[],nodez*zb);

        end
        
        c = colorbar('AxisLocation','in');
        c.Label.String = 'Temperature of melt (in C)';
        c.Limits = [40 70];
        m.FaceColor = 'interp';
        m.DiffuseStrength = 0.8;
        m.EdgeColor = [0.5, 0.5 0.5];
        m.EdgeAlpha = [0.5];
        view(3)
        xlabel('x')
        ylabel('y')
        zlabel('z')
        zlim([0 h*zb])
        xlim([0 h*xb])
        ylim([0 h*yb])
        drawnow        
        timestep = timestep +st;

    end
    
    
end
end