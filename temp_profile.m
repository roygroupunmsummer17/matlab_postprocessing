% This will plot the temperature profile
function temp_profile(Tscale,slicevx,slicevy,slicevz,number,...
                      base,file,slicer,xb,yb,zb,h,...
                      timestep,st,movie)

% Tscale is the temp scaling
% base is the location folder
% file is a scalar h5 file
% file3 is also a scalar file
% Tr is the node location
% h is the dimensionalization
% xb,yb,zb is the scaling for its respective axis
% nodes is the # of nodes

figure(number)

[X, ~, ~, shape] = read_geometry(base,file);   % reads the geometry

[sx, sy, sz] = size(X);
ux = 0:h*xb/(sx-1):h*xb;
uy = 0:h*yb/(sy-1):h*yb;
uz = 0:h*zb/(sz-1):h*zb;
spacingx = h/(sx-1);
spacingy = h/(sy-1);
spacingz = h/(sz-1);

nodex = slicevx * spacingx;
nodey = slicevy * spacingy;
nodez = slicevz * spacingz; 

hold on

if movie == 0

    tfluid = read_scalar(base,file,timestep,shape);
    tfluid = permute(tfluid,[2 1 3]);

    tfluid = Tscale * tfluid;

    if slicer == 1
        T = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);        

    elseif slicer == 2
        T = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);

    elseif slicer == 3
        T = slice(ux,uy,uz,tfluid,[],[],nodez*zb);

    end
    caxis([500 2e3])
    c = colorbar('AxisLocation','in');
    c.Label.String = 'Temperature of melt (in C)';
    c.Limits = [500 2e3];
    T.FaceColor = 'interp';
    T.DiffuseStrength = 0.8;
    T.EdgeColor = [0.5, 0.5 0.5];
    T.EdgeAlpha = [0.5];
    view(3)
    xlabel('x')
    ylabel('y')
    zlabel('z')
    zlim([0 h*zb])
    xlim([0 h*xb])
    ylim([0 h*yb])
    pause(.1)
    hold off

else
    
    for t = 0:floor(500/st)

        tfluid = read_scalar(base,file,timestep,shape);
        tfluid = permute(tfluid,[2 1 3]);

        tfluid = Tscale * tfluid;

        if slicer == 1
            T = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);     

        elseif slicer == 2
            T = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);


        elseif slicer == 3
            T = slice(ux,uy,uz,tfluid,[],[],nodez*zb);

        end
        
        c = colorbar('AxisLocation','in');
        c.Label.String = 'Temperature of melt (in C)';
        T.FaceColor = 'interp';
        T.DiffuseStrength = 0.8;
        T.EdgeColor = [0.5, 0.5 0.5];
        T.EdgeAlpha = [0.5];
        view(3)
        xlabel('x')
        ylabel('y')
        zlabel('z')
        zlim([0 h*zb])
        xlim([0 h*xb])
        ylim([0 h*yb])
        drawnow        
        timestep = timestep +st;

    end
    
    
end
end