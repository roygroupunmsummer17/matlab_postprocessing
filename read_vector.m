function [Vx Vy Vz] = read_vector(base, file, timestep, shape, scale)
%READ_VECTOR Read a field of 3D vectors from an HDF5 file in a base
%directory, at a given timestep, shape and optional scale factor (which can
%be a scalar or 3x1 column vector for individual scaling in each
%direction). The number of rows in the dataset must be 3, and the number of
%columns equal to the product of the shape dimensions.
%
%Return a 4D matrix, where V(:,x,y,z) is a 3x1 matrix containing the vector
%at point (x,y,z) in the field.  At least it's supposed to, if I don't get
%the ordering of the data set mixed up... :-)

  if nargin < 5
      scale = 1.0; % default if not passed in as an argument
  end
  [scale_rows, scale_cols] = size(scale);
%   if scale_rows == 1 && scale_cols == 1
%       scale = repmat(scale,3,1); % make scale into a column vector
%   elseif scale_rows ~= 3 || scale_cols ~= 1
%       error('Scale must be a scalar or 3x1 column vector.');
%   end

  data_set = ['/VisualisationVector/' num2str(timestep)];
  data = h5read([base file], data_set);

  % for scaling and reshaping, dimensions must agree, so check for that
  [data_rows, data_cols] = size(data);
  if data_rows ~= 3 || data_cols ~= prod(shape)
      error('Need 3x%d data set, got %dx%d.', prod(shape), data_rows, data_cols);
  end

  data = scale .* data;
  Vx = reshape(data(1,:), shape);
  Vy = reshape(data(2,:), shape);
  Vz = reshape(data(3,:), shape);
end

