function [SX, SY] = streamsave(vscale,slicevx,slicevy,slicevz,...
                     base,file1,file2,xb,yb,zb,h,muscale,Tb,k)

% Tscale is the temp scaling
% base is the location folder
% file is a scalar h5 file
% file3 is also a scalar file
% Tr is the node location
% h is the dimensionalization
% xb,yb,zb is the scaling for its respective axis
% nodes is the # of nodes

% close all

[X, ~, ~, shape] = read_geometry(base,file1);   % reads the geometry
[sx, sy, sz] = size(X);
spacingx = h/(sx-1);
spacingy = h/(sy-1);
spacingz = h/(sz-1);

nodex = slicevx * spacingx; %This is the slicing plane where the streamlines will start has to be between [0,1]
nodey = slicevy * spacingy; %This is the slicing plane where the streamlines will start has to be between [0,1]
nodez = slicevz * spacingz; %This is the slicing plane where the streamlines will start has to be between [0,1]

[x, y, z] = meshgrid(0:h*xb/(sx-1):h,0:h*yb/(sy-1):h*yb,0:h*zb/(sz-1):h*zb);
NN = zeros(sx,sy);
NN1 = zeros(sx,sy);
NN2 = zeros(sx,sy);
p = 1;

for timestep = [0 50 250 500]

[Vx, Vy, Vz] = read_vector(base, file1, timestep, shape);
Vx = Vx * vscale;
Vy = Vy * vscale;
Vz = Vz * vscale;
[Ux, Uy, Uz] = read_vector(base, file2, timestep, shape);
Ux = Ux * vscale;
Uy = Uy * vscale;
Uz = Uz * vscale;

rel_x = Vx - Ux;
rel_y = Vy - Uy;
rel_z = Vz - Uz;

k = 1;

% for i = 0:sx-1
%         s1 = streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
% 
%         loc1(k,1) = fix(s1.XData(end)/spacingx); 
%         loc1(k,2) = fix(s1.YData(end)/spacingy); 
% 
%         s2 = streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
% 
%         loc2(k,1) = fix(s2.XData(end)/spacingx); 
%         loc2(k,2) = fix(s2.YData(end)/spacingy); 
% 
%         k = k + 1;
% 
% end
k = 1;
for i = 0:sx-1
    for j = 0:sy-1
        s = streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,j*spacingy,nodez*zb);
       
        loc(k,1) = fix(s.XData(end)/spacingx); 
        loc(k,2) = fix(s.YData(end)/spacingy); 

        k = k + 1;

    end
end

loc = loc + 1;
locx = reshape(loc(:,1),[sx sx]);
locy = reshape(loc(:,2),[sy sy]);
% loc1 = loc1 + 1;
% loc1x = reshape(loc1(:,1),[sx 1]);
% loc1y = reshape(loc1(:,2),[sy 1]);
% loc2 = loc2 + 1;
% loc2x = reshape(loc2(:,1),[sx 1]);
% loc2y = reshape(loc2(:,2),[sy 1]);

for i = 1:sx
    for j = 1:sy
        NN(locx(i,j),locy(i,j)) = NN(locx(i,j),locy(i,j)) + 1;
    end
end


SX(:,p) = NN(:,9);
SY(:,p) = NN(9,:);
p = p+1;

% for i = 1:sx
%         NN1(loc1x(i),loc1y(i)) = NN1(loc1x(i),loc1y(i)) + 1;
%         NN2(loc2x(i),loc2y(i)) = NN2(loc2x(i),loc2y(i)) + 1;
% end

end
%% 3D Histogram

% subplot(3,2,[3 4])
% a = bar3(NN)
% colorbar
% xlabel('x')
% ylabel('y')
% xlim([.5 sx+.5])
% ylim([.5 sy+.5])
% zlabel('Frequency')
% title('Frequency of Streamlines')
% for k = 1:length(a)
%     zdata = a(k).ZData;
%     a(k).CData = zdata;
%     a(k).FaceColor = 'interp';
% end
% for i = 1:numel(a)
%     zData = get(a(i), 'ZData');  % Get the z data
%     index = logical(kron(zData(2:6:end, 2) == 0, ones(6, 1)));  % Find empty bars
%     zData(index, :) = nan;                 % Set the z data for empty bars to nan
%     set(a(i), 'ZData', zData);   % Update the graphics objects
% end
% 
% subplot(3,2,1)
% b = bar3(NN1)
% colorbar
% xlabel('x')
% ylabel('y')
% xlim([.5 sx+.5])
% ylim([.5 sy+.5])
% zlabel('Frequency')
% title('Frequency of Streamlines')
% for k = 1:length(b)
%     zdata = a(k).ZData;
%     b(k).CData = zdata;
%     b(k).FaceColor = 'interp';
% end
% for i = 1:numel(b)
%     zData = get(b(i), 'ZData');  % Get the z data
%     index = logical(kron(zData(2:6:end, 2) == 0, ones(6, 1)));  % Find empty bars
%     zData(index, :) = nan;                 % Set the z data for empty bars to nan
%     set(b(i), 'ZData', zData);   % Update the graphics objects
% end
% 
% subplot(3,2,2)
% c = bar3(NN2)
% colorbar
% xlabel('x')
% ylabel('y')
% xlim([.5 sx+.5])
% ylim([.5 sy+.5])
% zlabel('Frequency')
% title('Frequency of Streamlines')
% for k = 1:length(c)
%     zdata = a(k).ZData;
%     c(k).CData = zdata;
%     c(k).FaceColor = 'interp';
% end
% for i = 1:numel(c)
%     zData = get(c(i), 'ZData');  % Get the z data
%     index = logical(kron(zData(2:6:end, 2) == 0, ones(6, 1)));  % Find empty bars
%     zData(index, :) = nan;                 % Set the z data for empty bars to nan
%     set(c(i), 'ZData', zData);   % Update the graphics objects
% end

%% 2D Histogram
xx = (0:1:sx-1)*h/(sx-1);
close all
subplot(2,1,1)
hold on
colormap([ 0 0 0
           1 0 0
           1 1 0
           1 1 1])
a = area(xx,SX)

hold off

legend('time = 0','time = 1','time = 5','time = 10','Location','best')
xlabel('Position x')
ylabel('Frequency')
xlim([0 xx(end)])
ylim([0 30])
title(['Frequency of streamlines end point','\newline','mu = ',num2str(muscale),' and Tb = ',num2str(Tb),' and k = ',num2str(k),])

hold on
subplot(2,1,2)
colormap([ 0 0 0
           1 0 0
           1 1 0
           1 1 1])
a = area(xx,SY)

hold off

legend('time = 0','time = 1','time = 5','time = 10','Location','best')
xlabel('Position y')
ylabel('Frequency')
xlim([0 xx(end)])
ylim([0 30])
title(['Frequency of streamlines end point','\newline','mu = ',num2str(muscale),' and Tb = ',num2str(Tb),' and k = ',num2str(k),])

% saveas(gcf,(['Ext_Data/',base,'streamline_freq']),'fig')
% h = gcf;
% set(h,'Units','Inches');
% pos = get(h,'Position');
% set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
% 
% print(h,(['Ext_Data/',base,'streamline_freq']),'-dpdf','-r0')




end