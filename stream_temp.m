% Generic Quiver3 Plotting for given .h5 file 
% This code make a 'movie' of v_melt using the v_melt.h5 file
% and using the read_geometry.m and read_vector.m files to plot the given
% .h5 file

clear
close all

rho_0 = 3300;
alpha = 2.5e-5;
g     = 9.81;
kappa_0 = 1.E-6;
Tscale = 1500-27;
Tval   = 1573;
h      = 1e6;
muscale = 1e22;
Ra      = rho_0*alpha*g*Tscale*(h^3)/(kappa_0*muscale);
Rafac   = rho_0*alpha*g*Tscale*(h^3)/(kappa_0);
vscale  = rho_0*alpha*g*Tscale*(h^2)/muscale;
slicet = 5;
slicev = .5;

number = 1;

file = 'T_fluid.h5'; % you can change this for given .h5 file
file1 = 'v_melt.h5'; % you can change this for given .h5 file
file2 = 'v_solid.h5';
base = '/'; %base directory
slicer = 1; % chose what plane you want. 1 = yz 2 = zx 3 = xy
xb = 1;
yb = 1;
zb = .4;

nodex = slicev*h; %This is the slicing plane where the streamlines will start has to be between [0,1]
nodey = slicev*h; %This is the slicing plane where the streamlines will start has to be between [0,1]
nodez = slicev*h; %This is the slicing plane where the streamlines will start has to be between [0,1]

timestep = 0;  %initial drawing
st  = 10; %skip count
movie = 0;
figure(number)

[X, ~, ~, shape] = read_geometry(base,file);   % reads the geometry
[Vx Vy Vz] = read_vector(base, file1, timestep, shape);
Vx = Vx * vscale;
Vy = Vy * vscale;
Vz = Vz * vscale;
[Ux Uy Uz] = read_vector(base, file2, timestep, shape);
Ux = Ux * vscale;
Uy = Uy * vscale;
Uz = Uz * vscale;

rel_x = Vx - Ux;
rel_y = Vy - Uy;
rel_z = Vz - Uz;

[sx sy sz] = size(X);
spacingx = h/(sx-1);
spacingy = h/(sy-1);
spacingz = h/(sz-1);

[x y z] = meshgrid(0:h*xb/(sx-1):h,0:h*yb/(sy-1):h*yb,0:h*zb/(sz-1):h*zb);
ux = 0:h*xb/(sx-1):h*xb;
uy = 0:h*yb/(sy-1):h*yb;
uz = 0:h*zb/(sz-1):h*zb;

hold on
if movie == 0

    tfluid = read_scalar('/','T_fluid.h5',timestep,shape);
    tfluid = Tscale * tfluid;

    if slicer == 1
        node = ux(slicet); % this choses at which node point
        T = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);        
        for i = 0:sy-1
            streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
            drawnow
        end

    elseif slicer == 2
        node = uy(slicet); % this choses at which node point
        T = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);
        for i = 0:sx-1
            streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
            drawnow
        end

    elseif slicer == 3
        node = uz(slicet); % this choses at which node point
        T = slice(ux,uy,uz,tfluid,[],[],nodez*zb);
        for i = 0:sx-1
            for j = 0:sy-1
                streamline(x,y,z,rel_x,rel_y,rel_z,spacingx*i,spacingy*j,nodez*zb);
                drawnow
            end
        end
    end
    T.FaceColor = 'interp';
    T.DiffuseStrength = 0.8;
    T.EdgeColor = [0.5, 0.5 0.5];
    T.EdgeAlpha = [0.5];
    view(3)
    xlabel('x')
    ylabel('y')
    zlabel('z')
    zlim([0 h*zb])
    xlim([0 h*xb])
    ylim([0 h*yb])
    pause(.1)
    hold off

else
    
    for t = 0:floor(998/st)

        tfluid = read_scalar('/','T_fluid.h5',timestep,shape);
        tfluid = Tscale * tfluid;

        if slicer == 1
            node = ux(slicet); % this choses at which node point
            T = slice(ux,uy,uz,tfluid,nodex*xb,[],[]);        
            for i = 0:sy-1
                streamline(x,y,z,rel_x,rel_y,rel_z,nodex*xb,i*spacingy,nodez*zb);
                drawnow
            end

        elseif slicer == 2
            node = uy(slicet); % this choses at which node point
            T = slice(ux,uy,uz,tfluid,[],nodey*yb,[]);
            for i = 0:sx-1
                streamline(x,y,z,rel_x,rel_y,rel_z,i*spacingx,nodey*yb,nodez*zb);
                drawnow
            end

        elseif slicer == 3
            node = uz(slicet); % this choses at which node point
            T = slice(ux,uy,uz,tfluid,[],[],nodez*zb);
            for i = 0:sx-1
                for j = 0:sy-1
                    streamline(x,y,z,rel_x,rel_y,rel_z,spacingx*i,spacingy*j,nodez*zb);
                    drawnow
                end
            end
        end

        T.FaceColor = 'interp';
        T.DiffuseStrength = 0.8;
        T.EdgeColor = [0.5, 0.5 0.5];
        T.EdgeAlpha = [0.5];
        view(3)
        xlabel('x')
        ylabel('y')
        zlabel('z')
        zlim([0 h*zb])
        xlim([0 h*xb])
        ylim([0 h*yb])
        hold off
        
        timestep = timestep +st;

    
    end
    
    
    
    
     
    
    
    
    
    
    
end