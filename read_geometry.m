function [ X, Y, Z, shape ] = read_geometry(base, file, scale)
%READ_GEOMETRY Read a mesh geometry from an HDF5 file in a base directory,
%assuming 3D-coordinates; scale is a scalar or 3x1 column vector containing
%optional scaling factors for each dimension (if it's a scalar, uses the
%same scaling for each dimension).
%
%Return a 3D "meshgrid" defining X, Y, and Z coordinates for each point in
%the grid.  This *should* work with grids that have unequal sizes in each
%direction, but has only been tested with equal sizes.  Also returns the
%shape of the grid which can be used to reshape linear vectors read from
%the same HDF5 file to conform to the grid.

  if nargin < 3
      scale = 1.0; % default if not passed in as an argument
  end
  [scale_rows, scale_cols] = size(scale);
%   if scale_rows == 1 && scale_cols == 1
%       scale = repmat(scale,1,3); % make scale into a column vector
%   elseif scale_rows ~= 3 || scale_cols ~= 1
%       error('Scale must be a scalar or 3x1 column vector.');
%   end

  geom = h5read([base file], '/Mesh/0/mesh/geometry');
  
  % for scaling, dimensions must agree, so check for that
  dims = size(geom,1);
  if dims ~= 3
      error('Need 3 dimensions, got %d.', dims);
  end
  
  geom = scale .* geom;
  x = geom(1,:);
  y = geom(2,:);
  z = geom(3,:);
  
  x_stride = find(x(2:end) < x(1:end-1), 1);
  y_stride = find(y(2:end) < y(1:end-1), 1);
  shape = [x_stride, y_stride / x_stride, length(x) / y_stride];
  
  X = reshape(x, shape);
  Y = reshape(y, shape);
  Z = reshape(z, shape);
end
