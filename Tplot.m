function Tplot(Tscale,base,file,change,xb,yb,zb,...
               Tr,st,symbol,line,fill,number) 

% Tscale is the temp scaling
% base is the location folder
% file is a scalar h5 file
% Tr is the node location
% st is the skip, it helps view the grpah better
% line decides whether the symbol will have an outline
% fill decided whether the symbol will be filled

[X, ~, ~, shape] = read_geometry(base,file);   % reads the geometry
X = size(X);

hold on
        Tr(1) = fix(Tr(1) / change / xb) + 1;
        Tr(2) = fix(Tr(2) / change / yb) + 1;
        Tr(3) = fix(Tr(3) / change / zb) + 1;
j= 0;

    for i = 0:floor(500/st)
        j = i*st;
        tfluid = read_scalar(base,file,j,shape);
        tfluid = Tscale * tfluid;
        Ts(i+1,1) = 2 * i*st / 1e2;

        Ts(i+1,2) = tfluid(Tr(1),Tr(2),Tr(3)); 

    end
    
hh = plot(Ts(:,1),Ts(:,2),symbol);

set(hh(1),'MarkerEdgeColor',line,'MarkerFaceColor',fill,'Markersize',number)

  
    
    
clear all
end