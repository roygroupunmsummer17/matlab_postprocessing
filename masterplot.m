%% MATLAB Post-Processing
% This script is the "Master file" that utilizes the functions
% shown below and plots/saves data depending on the function you chose
% If the comment line starts with a lower case letter, then it is a
% subsequent line of the previous comment line; otherwise, its standalone
% A simple description of each function is described below

%% Initialization

clear all

% This for loop is specifically for plotting at different timesteps
% must be placed after clear but before close
for pp = [0]
for mm = [1e20]% 1e21 5e21 1e22]
for nn = [500]% 50 250 500]

close all

% Constants extracted from simulation_no_git.py - may be outdated

rho_0 = 3300;           % Constant
alpha = 2.5e-5;         % Constant
g     = 9.81;           % Constant
kappa_0 = 1.E-6;        % Constant



xb = 1;                 % These three # scale the x,y,z direction to the
yb = 1;                 % amount they are in the python code. Can be diff
zb = .4;                % in subsequent runs.

vx = 514933;            % These three # will decide where a slicing plane
vy = 512598;            % will occur. This number must be between 0 and the
vz = 200000;            % limit of the respective axis. 

timestep = nn;           % What timestep everything will be displayed
st  = 1;                % Skip count for timestepping
movie = 0;              % If == 1, then timestep will be ignored, all timestep
save = 0;               % 0 = no save | 1 = save of current fig
number = 1;             % Decided the fig # that MATLAB will use
slicer = 1;             % Slices desired plane: 1 = yz 2 = zx 3 = xy

plotting = pp;           % Plotting will plot temp_prof = 0 or mu_prof = 1
nodes = 16;             % # of Nodes
k = '0.001';              % Choose k for different folders (format:'#.#')
rho = '3000';           % Choose rho for different folders (format:'####')
muscale = mm;        % Choose mu for different folders (format:#e+##)
Tb = '1300.0';
Tscale = 1500-27;       % The scaling for temperature
h      = 1e6;           % The scaling for distance
vscale  = rho_0*alpha*g*Tscale*(h^2)/muscale;   % The scaling for velocity
tau = h/vscale;         % The scaling for time

slicevx = round(vx/(h*xb/nodes));               % Conversion of dimensional
slicevy = round(vy/(h*yb/nodes));               % to nondimensional #
slicevz = round(vz/(h*zb/nodes));               % for easier calculation

%% Files and Locations

file = 'T_fluid.h5';    % Desired h5 file to be read, must be scalar
file1 = 'v_melt.h5';    % Desired h5 files to be read, must be velocity
file2 = 'v_solid.h5';   % vectors
file3 = 'mu.h5';        % Only used when using Tsave

base = (['output_',num2str(nodes),'_rho',rho,...
         '/mu=',num2str(muscale),'/Tb=',Tb,'/k=',k,'/']);
     
% For base, you must actively change nodes, rho, muscale, and k to get into
% the correct folder of your choosing. One simply mistake in misformatting
% or wrong value, and the entire code won't run because they all depend on
% base.

% !!!! For windows change / to \ !!!!

%% Point Location

% This little snippet from change to set makes 4 points that tell where
% data is being recorded for Tsave. It is more for visualization purposes
% and must be run before anything because plot3 deletes whats in the
% current fig. You choose where you want the points to be located at. Their
% position is described by xx, yy, zz but you must put them in node form.
% Normally this should be commented out

change = h/nodes;       % A global spacing for plot3
subplot(2,1,1)
streamsave(vscale,slicevx,slicevy,slicevz,base,file1,file2,...
           xb,yb,zb,h,muscale,Tb,k)

subplot(2,1,2)
xx = change * xb * [8 8 6 10];
yy = change * yb * [6 10 8 8];
zz = change * zb * [14 14 14 14];
hold on
% hh1 = plot3(xx(1),yy(1),zz(1),'o');
% set(hh1(1),'MarkerEdgeColor','none','MarkerFaceColor','k','Markersize',8)
% hh2 = plot3(xx(2),yy(2),zz(2),'o');
% set(hh2(1),'MarkerEdgeColor','k','MarkerFaceColor','none','Markersize',12)
% hh3 = plot3(xx(3),yy(3),zz(3),'s');
% set(hh3(1),'MarkerEdgeColor','none','MarkerFaceColor','r','Markersize',18)
% hh4 = plot3(xx(4),yy(4),zz(4),'d');
% set(hh4(1),'MarkerEdgeColor','none','MarkerFaceColor','b','Markersize',18)

%% Calling of Functions

hold on                 % Always good

% Here are all the functions and a basic description of what they are used
% for. For a more detailed version read the comments in the functions

% Temp_profile plots a slice of the temperature field. The plane that is
% sliced depends on what slicer and slicev is. If movie is one, this will
% plot all timesteps for that specific slice. However, temp_mov is
% recomended for multiple slices and streamlines.
% 

if plotting == 0
    temp_profile(Tscale,slicevx,slicevy,slicevz,number,base,...
                 file,slicer,xb,yb,zb,h,timestep,st,movie)
    temp_profile(Tscale,slicevx,slicevy,slicevz,number,base,...
                 file,slicer+1,xb,yb,zb,h,timestep,st,movie)
else
    mu_profile(muscale,slicevx,slicevy,slicevz,number,base,...
               file3,slicer,xb,yb,zb,h,timestep,st,movie)
    mu_profile(muscale,slicevx,slicevy,slicevz,number,base,...
               file3,slicer+1,xb,yb,zb,h,timestep,st,movie)
end

% Streamlines plots streamlines at the nodes. If you want
% to change the quantity/location of streamlines then you must edit the
% code, but it is rather easy. The strealines will be located at slicev()
% depending on what slicer.

streamlines(vscale,slicevx,slicevy,slicevz,number,base,...
            file1,file2,slicer,xb,yb,zb,h,timestep,st,movie,'k')
streamlines(vscale,slicevx,slicevy,slicevz,number,base,...
            file1,file2,slicer+1,xb,yb,zb,h,timestep,st,movie,'k')

% Streamcircle plots streamlines around a radius the the temp_profile for
% visualization purposes. The radius can be editted but you must edit that
% within the function itself.

streamcircle(vscale,slicevz,number,base,file1,file2,xb,yb,zb,h,timestep,'k')
hold off
% Temp_mov is temp_profile and streamlines put together. It plots the
% streamlines and slice of the temperature over all timesteps. If you want
% to edit the # of streamlines or # of temp_profile slices, you must do
% that inside the function.

% temp_mov(Tscale,slicevx,slicevy,slicevz,number,base,file,file1,file2,...
%          slicer,xb,yb,zb,h,vscale,timestep,st,movie,tau)

% Streamsave records the ending position of all streamlines for several
% timesteps and plots them as a bar graph. This is for viualization
% purposes.


% The following functions are for saving and plotting the temperature for
% all timesteps. The temperature will be recorded at node Tr#. Tplot plots
% the recorded data, but can be run stand alone. Tplot(1) is upwind,
% Tplot(2) is downwind, and Tplot(3/4) are neither. Their color and fill
% represent what they are. Since there are many timesteps, currently ~1000,
% you will need a skip #, which is st in this code. This will make the
% graphs more readable and understandable. 
% figure

% close all
% 
% Tr1 = [xx(1) yy(1) zz(1)];
% Tr2 = [xx(2) yy(2) zz(2)];
% Tr3 = [xx(3) yy(3) zz(3)];
% Tr4 = [xx(4) yy(4) zz(4)];
% 
% Tsave(Tscale,base,file,file3,Tr1,h,xb,yb,zb,nodes,muscale)
% Tsave(Tscale,base,file,file3,Tr3,h,xb,yb,zb,nodes,muscale)
% Tsave(Tscale,base,file,file3,Tr2,h,xb,yb,zb,nodes,muscale)
% Tsave(Tscale,base,file,file3,Tr4,h,xb,yb,zb,nodes,muscale)
% 
% st = 10;
% 
% Tplot(Tscale,base,file,change,xb,yb,zb,Tr3,st,'s','r','r',12)
% Tplot(Tscale,base,file,change,xb,yb,zb,Tr4,st,'d','b','b',12)
% Tplot(Tscale,base,file,change,xb,yb,zb,Tr1,st,'o','none','k',8)
% Tplot(Tscale,base,file,change,xb,yb,zb,Tr2,st,'o','k','none',12)


%% Plotting and Saving

% Anything down here is up to the person running the code. You can change
% the title, the limit, the legend, and where and how the images are saved

time = (timestep) * 2 / 1e2;
% 

%% Temp_profile && mu_profile w Strealines and Streamcircle

% if plotting == 0
% figure(number)
% 
%     title(['Temperature slice for ',num2str(time),' Million Years','\newline','mu = ',num2str(muscale),' and Tb = ',num2str(Tb),' and k = ',num2str(k),])
% 
%     saveas(gcf,(['Ext_Data/' base,'Temp_time=',num2str(time)]),'fig')
%     
%     h = gcf;
%     set(h,'Units','Inches');
%     pos = get(h,'Position');
%     set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
%     
%     print(h,(['Ext_Data/' base,'Temp_time=',num2str(time)]),'-dpdf','-r0')
% else
%     title(['Mu slice for ',num2str(time),' Million Years','\newline','mu = ',num2str(muscale),' and Tb = ',num2str(Tb),' and k = ',num2str(k),])
% 
%     saveas(gcf,(['Ext_Data/' base,'mu_time=',num2str(time)]),'fig')
%     
%     h = gcf;
%     set(h,'Units','Inches');
%     pos = get(h,'Position');
%     set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
%     
%     print(h,(['Ext_Data/' base,'mu_time=',num2str(time)]),'-dpdf','-r0')
% end
    

%% Tsave and Tplot
% legend(['Node ',num2str(Tr1)],['Node ',num2str(Tr2)],...
%        ['Node ',num2str(Tr3)],['Node ',num2str(Tr4)])
% 
% legend('Location','best')
% 
% 
% hold off
% % 
% time = (timestep) * 2 / 1e2;
% % 
% 
% title(['Temperature Evolution plot','\newline','mu = ',num2str(muscale),' and Tb = ',num2str(Tb),' and k = ',num2str(k),])
% xlabel('Time t in Myrs')
% zlabel('Temperature in C')
% ylim([1000 2000])
% 
%     saveas(gcf,(['Ext_Data/' base,'Temp_evo_skip=',num2str(st)]),'fig')
%     h = gcf;
%     set(h,'Units','Inches');
%     pos = get(h,'Position');
%     set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
%     
%     print(h,(['Ext_Data/' base,'Temp_evo_skip=',num2str(st)]),'-dpdf','-r0')

%% Ends for For-Loops above

% end
% pause(.1)
end               % This is the end for the for loop for nn
end
end



