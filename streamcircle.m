function streamcircle(vscale,slicevz,number,...
                     base,file1,file2,xb,yb,zb,h, ...
                     timestep,color)

% Tscale is the temp scaling
% base is the location folder
% file is a scalar h5 file
% file3 is also a scalar file
% Tr is the node location
% h is the dimensionalization
% xb,yb,zb is the scaling for its respective axis
% nodes is the # of nodes

figure(number)

[X, ~, ~, shape] = read_geometry(base,file1);   % reads the geometry
[Vx, Vy, Vz,] = read_vector(base, file1, timestep, shape);
Vx = Vx * vscale;
Vy = Vy * vscale;
Vz = Vz * vscale;
[Ux, Uy, Uz] = read_vector(base, file2, timestep, shape);
Ux = Ux * vscale;
Uy = Uy * vscale;
Uz = Uz * vscale;

rel_x = Vx - Ux;
rel_y = Vy - Uy;
rel_z = Vz - Uz;

[sx, sy, sz] = size(X);
spacingx = h/(sx-1);
spacingy = h/(sy-1);
spacingz = h/(sz-1);

nodez = slicevz * spacingz; %This is the slicing plane where the streamlines will start has to be between [0,1]

[x, y, z,] = meshgrid(0:h*xb/(sx-1):h,0:h*yb/(sy-1):h*yb,0:h*zb/(sz-1):h*zb);

hold on

        for i = 1:3
            for j = 1:3
                hold on
                p = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 + spacingx*i,h/2 + spacingy*j,nodez*zb);
                plot3(p.XData(end),p.YData(end),p.ZData(end),'Color',color,'MarkerSize',4,'Marker','o','MarkerFaceColor',color);
                q = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 - spacingx*i,h/2 + spacingy*j,nodez*zb);
                plot3(q.XData(end),q.YData(end),q.ZData(end),'Color',color,'MarkerSize',4,'Marker','o','MarkerFaceColor',color);
                r = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 + spacingx*i,h/2 - spacingy*j,nodez*zb);
                plot3(r.XData(end),r.YData(end),r.ZData(end),'Color',color,'MarkerSize',4,'Marker','o','MarkerFaceColor',color);
                s = streamline(x,y,z,rel_x,rel_y,rel_z,h/2 - spacingx*i,h/2 - spacingy*j,nodez*zb);
                plot3(s.XData(end),s.YData(end),s.ZData(end),'Color',color,'MarkerSize',4,'Marker','o','MarkerFaceColor',color);

                set( p, 'Color', [0 0 0] )
                set( q, 'Color', [0 0 0] )
                set( r, 'Color', [0 0 0] )
                set( s, 'Color', [0 0 0] )

            end
        end
        
    hold off

end