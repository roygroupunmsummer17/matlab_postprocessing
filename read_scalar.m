function S = read_scalar(base, file, timestep, shape, scale)
%READ_SCALAR Read a grid of scalar values from an HDF5 file in a base
%directory, at a given timestep, shape and optional scale factor. The
%number of rows in the dataset must be 1, and the number of columns equal
%to the product of the shape dimensions.
%
%Return a 3D matrix containing the scalar values at each coordinate point.

  if nargin < 5
      scale = 1.0; % default if not passed in as an argument
  end
  [scale_rows, scale_cols] = size(scale);
  if scale_rows ~= 1 || scale_cols ~= 1
      error('Scale must be a scalar.');
  end

  data_set = ['/VisualisationVector/' num2str(timestep)];
  data = h5read([base file], data_set);
  
  % for scaling and reshaping, dimensions must agree, so check for that
  [data_rows, data_cols] = size(data);
  if data_rows ~= 1 || data_cols ~= prod(shape)
      error('Need 1x%d data set, got %dx%d.', prod(shape), data_rows, data_cols);
  end
  
  data = scale .* data;
  S = reshape(data, shape);
end
